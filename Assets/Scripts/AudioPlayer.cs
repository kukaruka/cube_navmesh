using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    public static List<AudioClip> boosterEffects;
    public static List<AudioClip> damageEffects;
    public List<AudioClip> clips;
    private List<string> sounds;

    public static AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        boosterEffects = new List<AudioClip>();
        damageEffects = new List<AudioClip>();

        var resourceAudioClips = Resources.LoadAll("LaserWeaponsSoundPack/Booster", typeof(AudioClip));
        foreach (AudioClip clip in resourceAudioClips)
        {
            boosterEffects.Add(clip);
        }

        resourceAudioClips = Resources.LoadAll("LaserWeaponsSoundPack/Damage", typeof(AudioClip));
        foreach (AudioClip clip in resourceAudioClips)
        {
            damageEffects.Add(clip);
        }

        audioSource = GetComponent<AudioSource>();
        sounds = new List<string>();
        sounds.Add(clips[0].name);
        sounds.Add(clips[1].name);
        sounds.Add(clips[2].name);
        System.Random rnd = new System.Random();
        int r = rnd.Next(0, sounds.Count);
        PlayAudioClip((string)sounds[r]);
    }

    // Update is called once per frame
    void Update()
    {
        if (!audioSource.isPlaying)
        {
            System.Random rnd = new System.Random();
            int r = rnd.Next(0, sounds.Count);
            PlayAudioClip((string)sounds[r]);
        }
    }

    public void PlayAudioClip(string clipToPlay)
    {
        foreach (AudioClip clip in clips)
        {
            if (clip.name == clipToPlay)
            {
                audioSource.clip = clip;
                if (!audioSource.isPlaying)
                    audioSource.Play();
            }
        }
    }
}