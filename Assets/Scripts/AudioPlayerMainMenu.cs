using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayerMainMenu : MonoBehaviour
{
    public List<AudioClip> clips;
    private AudioPlayer m_Player;
    private List<string> sounds;

    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {

        audioSource = GetComponent<AudioSource>();
        sounds = new List<string>();
        sounds.Add("main_menu");
        PlayAudioClip((string)sounds[0]);

    }

    // Update is called once per frame
    void Update()
    {
    }
    public void PlayAudioClip(string clipToPlay)
    {
        foreach (AudioClip clip in clips)
        {
            if (clip.name == clipToPlay)
            {
                audioSource.clip = clip;
                if (!audioSource.isPlaying)
                    audioSource.Play();
            }
        }
    }
}