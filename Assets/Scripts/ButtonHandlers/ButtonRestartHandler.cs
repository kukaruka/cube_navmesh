using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonRestartHandler : MonoBehaviour
{
    public void restartCallback()
    {
        GameOverScript.Restart();
    }
}
