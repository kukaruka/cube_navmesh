
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

using UnityEngine.InputSystem;
using static System.Math;
using UnityEngine.AI;
public class CollisionScore : MonoBehaviour
{
    public static bool isShifting = false;
    private MeshRenderer cubeMeshRenderer;
    private float lerpTime = 10.0f;
    [SerializeField] Color[] myColors;
    [SerializeField] Color[] hpColors;

    static Color[] colors;

    public static bool isHit = false;

    int colorIndex = 0;
    float t = 0f;
    int len;

    private float colorUpdateTime = 0.0f;
    private float colorShiftTime = 1.0f;
    private Material baseMaterial = null;
    public void Start()
    {
        isShifting = false;
        lerpTime = 10.0f;
        isHit = false;
        colorIndex = 0;
        t = 0f;
        colorUpdateTime = 0.0f;
        colorShiftTime = 1.0f;

        cubeMeshRenderer = GetComponent<MeshRenderer>();
        baseMaterial = new Material(cubeMeshRenderer.material);
        len = myColors.Length;
        colors = new Color[len];

        int i = 0;
        foreach (Color color in myColors)
        {
            colors[i] = color;
            i++;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            PlayerMovement.isHitByObstacle = false;
            var rb = GameObject.Find("CubeRigidbody").GetComponent<Rigidbody>();
            rb.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);

        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            var rb = GameObject.Find("CubeRigidbody").GetComponent<Rigidbody>();
            rb.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            PlayerMovement.isHitByObstacle = true;
            PlayerMovement.stopCor = true;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Obstacle"))
        {
            len = hpColors.Length;
            colors = new Color[len];
            int i = 0;
            foreach (Color color in hpColors)
            {
                colors[i] = color;
                i++;
            }
            ScoreScript.Health -= 1;
            ScoreScript.DisplayDamageText();
            isShifting = true;
            colorUpdateTime = 0.0f;

            var rb = GameObject.Find("CubeRigidbody").GetComponent<Rigidbody>();
            rb.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            PlayerMovement.isHitByObstacle = true;
            PlayerMovement.stopCor = true;

            System.Random rnd = new System.Random();
            int r = rnd.Next(0, 4);
            AudioPlayer.audioSource.PlayOneShot(AudioPlayer.damageEffects[r], 1.0f);
        }

        if (collision.gameObject.CompareTag("Booster"))
        {
            len = myColors.Length;
            colors = new Color[len];
            int i = 0;
            foreach (Color color in myColors)
            {
                colors[i] = color;
                i++;
            }

            ScoreScript.Score += 1;
            Destroy(collision.gameObject);
            isShifting = true;
            colorUpdateTime = 0.0f;

            System.Random rnd = new System.Random();
            int r = rnd.Next(0, 25);
            float vol;
            if (SceneManager.GetActiveScene().name == "SingleCubeNavMesh")
                vol = 0.25f;
            else
                vol = 1.0f;
            AudioPlayer.audioSource.PlayOneShot(AudioPlayer.boosterEffects[r], vol);

        }

        if (collision.gameObject.CompareTag("Enemy"))
        {
            len = hpColors.Length;
            colors = new Color[len];
            int i = 0;
            foreach (Color color in hpColors)
            {
                colors[i] = color;
                i++;
            }
            ScoreScript.Health -= 1;
            ScoreScript.DisplayDamageText();
            isShifting = true;
            colorUpdateTime = 0.0f;
            HunterMovement.stopCor = true;

            System.Random rnd = new System.Random();
            int r = rnd.Next(0, 4);
            AudioPlayer.audioSource.PlayOneShot(AudioPlayer.damageEffects[r], 1.0f);

        }
        /*
        if (collision.gameObject.CompareTag("Landmine"))
        {
            ScoreScript.Health -= 1;
            Destroy(gameObject);
        }*/
    }

    public void Update()
    {
        if (isShifting)
        {
            try
            {
                cubeMeshRenderer.material.color = Color.Lerp(cubeMeshRenderer.material.color, colors[colorIndex], lerpTime * Time.deltaTime);
                cubeMeshRenderer.material.SetColor("_EmissionColor", Color.Lerp(cubeMeshRenderer.material.color, colors[colorIndex], lerpTime * Time.deltaTime));
            }
            catch
            {
                ;
            }
            
            t = Mathf.Lerp(t, 1f, lerpTime * Time.deltaTime);
            if (t >= 0.9f)
            {
                colorIndex++;
                t = 0f;
                if (colorIndex++ > len)
                {
                    isShifting = false;
                }
                colorIndex = (colorIndex >= len) ? 0 : colorIndex;
            }
        }

        colorUpdateTime += Time.deltaTime;
        if (colorUpdateTime > colorShiftTime && isShifting)
        {
            isShifting = false;
            ScoreScript.DisplayNormalHealthText();
            cubeMeshRenderer.material = new Material(baseMaterial);
        }
    }
}