using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

using UnityEngine.InputSystem;
using static System.Math;
using UnityEngine.AI;

public class GameOverScript : MonoBehaviour
{
    public TextMeshProUGUI endGameText;
    public GameObject GameOverPanel;

    public static bool endgame = false;
    private void Start()
    {
        endGameText.enabled = false;
        GameOverPanel.SetActive(false);
    }
    void Update()
    {
        endGameText.enabled = false;
        GameOverPanel.SetActive(false);

        if (ScoreScript.Score == 0)
        {
            endgame = false;
        }
            

        if (ScoreScript.Health <= 0)
        {
            
            endGameText.enabled = true;
            GameOverPanel.SetActive(true);
            NavMeshAgent hunter = GameObject.Find("HunterAgent").GetComponent<NavMeshAgent>();
            hunter.isStopped = true;
            NavMeshAgent player = GameObject.Find("CubeAgent").GetComponent<NavMeshAgent>();
            player.isStopped = true;

            ScoreScript.DisplayDamageText();

            if (!endgame)
            {
                
            }
            
            endgame = true;
        }
    }
    public static void Restart()
    {

        ScoreScript.Score = 0;
        ScoreScript.Health = 3;

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}