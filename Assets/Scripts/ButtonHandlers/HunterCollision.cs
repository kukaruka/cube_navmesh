
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static System.Random;

public class HunterCollision : MonoBehaviour
{
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            HunterMovement.isHitByObstacle = false;
            var rb = GameObject.Find("HunterRigidbody").GetComponent<Rigidbody>();
            rb.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);

        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            var rb = GameObject.Find("HunterRigidbody").GetComponent<Rigidbody>();
            rb.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            HunterMovement.isHitByObstacle = true;
            HunterMovement.stopCor = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Booster"))
        {
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("Obstacle"))
        {
            HunterMovement.isHitByObstacle = false;
            var rb = GameObject.Find("HunterRigidbody").GetComponent<Rigidbody>();
            rb.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
            HunterMovement.isHitByObstacle = true;
            HunterMovement.stopCor = true;
        }
        /*
        if (collision.gameObject.CompareTag("Landmine"))
        {
            ScoreScript.Health -= 1;
            Destroy(gameObject);
        }*/
    }
}