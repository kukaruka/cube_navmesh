using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MiscButtonHandlers : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            Debug.Log("Restart");
            GameOverScript.Restart();
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            Debug.Log("Exit");
            SceneManager.LoadScene("CubeMainMenu");
        }
    }
}
