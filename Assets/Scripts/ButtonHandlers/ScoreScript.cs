using UnityEngine;
using TMPro;

public class ScoreScript : MonoBehaviour
{
    static public int Health = 3;
    static public int Score = 0;
    public static TextMeshProUGUI ScoreText;
    public static TextMeshProUGUI HealthText;
    void Start()
    {
        Health = 3;
        Score = 0;
        ScoreText = GameObject.Find("ScoreText").GetComponent<TextMeshProUGUI>();
        HealthText = GameObject.Find("HealthText").GetComponent<TextMeshProUGUI>();
    }
    void Update()
    {
        ScoreText.text = "Score: " + Score.ToString();
        if (Health < 0)
            Health = 0;
        HealthText.text = "Health: " + Health.ToString();
    }

    public static void DisplayDamageText()
    {
        HealthText.faceColor = new Color32(255, 0, 0, 255);
        HealthText.outlineColor = new Color32(255, 0, 0, 255);
    }

    public static void DisplayNormalHealthText()
    {
        HealthText.faceColor = new Color32(0, 222, 225, 255);
        HealthText.outlineColor = new Color32(0, 4, 255, 255);
    }
}

