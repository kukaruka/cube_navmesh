using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorLerpScript : MonoBehaviour
{
    Color lerpedColor = Color.white;

    void Update()
    {
        Color primaryColor = new Color(255f, 0f, 0f);
        Color secondaryColor = new Color(250f, 200f, 15f);

        lerpedColor = Color.Lerp(primaryColor, secondaryColor, Mathf.PingPong(Time.time, 1));
    }
}
