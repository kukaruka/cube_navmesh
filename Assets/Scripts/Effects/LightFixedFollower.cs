using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFixedFollower : MonoBehaviour
{
    private Transform mTransform;
    // Start is called before the first frame update
    void Start()
    {
        mTransform = GameObject.Find("CubeRigidbody").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(mTransform.localPosition.x,
                                         0.1f,
                                         mTransform.localPosition.z);
    }
}