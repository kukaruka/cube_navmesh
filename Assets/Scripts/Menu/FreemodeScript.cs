using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class FreemodeScript : MonoBehaviour
{
    public void EnterFreemodeButtonCallback()
    {
        Debug.Log("Starting freemode");
        SceneManager.LoadScene("SingleCubeNavMesh");
    }

    public void EnterHuntermodeButtonCallback()
    {
        Debug.Log("Starting huntermode");
        SceneManager.LoadScene("SingleCubeHunting");
    }
    public void EnterSirvivalmodeButtonCallback()
    {
        Debug.Log("Starting sirvival mode");
        SceneManager.LoadScene("SingleCubeDynamicObstacles");
    }
}
