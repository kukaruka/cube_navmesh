using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentMovement : MonoBehaviour
{
    GameObject cube = null;
    void Start()
    {
        cube = null;
        cube = GameObject.Find("CubeRigidbody");
    }
    void Update()
    {
        transform.localPosition = cube.transform.localPosition;
    }
}
