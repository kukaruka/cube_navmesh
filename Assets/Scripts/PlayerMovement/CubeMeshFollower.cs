using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMeshFollower : MonoBehaviour
{
    private Transform mTransform;
    // Start is called before the first frame update
    void Start()
    {
        mTransform = GameObject.Find("CubeAgent").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(mTransform.transform.localPosition.x,
            mTransform.transform.localPosition.y,
            mTransform.transform.localPosition.z);

        transform.localRotation = Quaternion.Euler(0,0,0);
    }
}
