using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static System.Math;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class HunterMovement : MonoBehaviour
{
    private Vector3 direction;
    private Vector3 desiredVelocity;
    public float tumblingDuration = 1.0f;
    public static bool isHitByObstacle = false;
    private NavMeshAgent agent = null;
    private GameObject cube = null;
    private GameObject hunter = null;
    private Coroutine cor;
    public static bool stopCor = false;
    private Rigidbody rb;

    private Vector3 dir;
    // Start is called before the first frame update

    void Start()
    {
        tumblingDuration = 1.0f;
        agent = null;
        cube = null;
        hunter = null;

        rb = GetComponent<Rigidbody>();
        isTumbling = false;
        stopCor = true;
        agent = GameObject.Find("HunterAgent").GetComponent<NavMeshAgent>();
        cube = GameObject.Find("CubeRigidbody");
        hunter = GameObject.Find("HunterRigidbody");
        direction = new Vector3(0, 0, 0);
        desiredVelocity = new Vector3(0, 0, 0);
    }
    void Update()
    {
        if (stopCor && cor != null)
        {
            if (cor != null)
                StopCoroutine(cor);
            stopCor = false;
            isTumbling = false;
            if (!isHitByObstacle)
            {
                transform.localPosition = new Vector3(0, 0.1f, 0);
            }
            transform.localRotation = Quaternion.Euler(new Vector3(0, 0.0f, 0));
            var rb = GetComponent<Rigidbody>();
            rb.velocity = new Vector3(0, 0, 0);
            rb.angularVelocity = new Vector3(0, 0, 0);
        }
            
        agent.transform.localPosition = hunter.transform.localPosition;
        agent.SetDestination(cube.transform.position);

        direction = agent.destination - transform.position;
        desiredVelocity = new Vector3(agent.desiredVelocity.x, agent.desiredVelocity.y, agent.desiredVelocity.z);

        dir = Vector3.zero;

        if (!GameOverScript.endgame && !isHitByObstacle)
        {
            if (Mathf.Abs(agent.remainingDistance) > 0.0)
            {
                if (Mathf.Abs(desiredVelocity.x) < Mathf.Abs(desiredVelocity.z))
                {
                    float sign = Mathf.Sign(desiredVelocity.z);
                    if (sign > 0)
                    {
                        dir = Vector3.forward; //fwd
                    }
                    else
                    {
                        dir = Vector3.back;
                    }
                }
                else
                {
                    float sign = Mathf.Sign(desiredVelocity.x);
                    if (sign > 0)
                    {
                        dir = Vector3.right;
                    }
                    else
                    {
                        dir = Vector3.left;
                    }
                }
            }
        }
        if (dir != Vector3.zero && !isTumbling)
        {
            if (SceneManager.GetActiveScene().name == "SingleCubeDynamicObstacles")
                tumblingDuration = 0.11f;
            else
                tumblingDuration = 0.14f;
            
            cor = StartCoroutine(Tumble(dir));
        }
    }

    public static bool isTumbling = false;
    IEnumerator Tumble(Vector3 direction)
    {
        isTumbling = true;
        var cubeSide = transform.localScale.x;

        var rotAxis = Vector3.Cross(Vector3.up, direction);
        var pivot = (transform.position + Vector3.down * transform.localScale.x / 2) + direction * transform.localScale.x / 2;

        var startRotation = transform.rotation;
        var endRotation = Quaternion.AngleAxis(90.0f, rotAxis) * startRotation;

        var startPosition = transform.position;
        var endPosition = transform.position + direction * transform.localScale.x;

        var rotSpeed = 90.0f / tumblingDuration;
        var t = 0.0f;

        while (t < tumblingDuration)
        {
            t += Time.deltaTime;
            if (t < tumblingDuration)
            { 
                rb.useGravity = false;
                transform.RotateAround(pivot, rotAxis, rotSpeed * Time.deltaTime);
                yield return null;
            }
            else
            {
                rb.useGravity = true;
                transform.rotation = endRotation;
                transform.position = endPosition;
            }
        }
        isTumbling = false;
    }
}