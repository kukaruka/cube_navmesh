using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public static bool isControlledByMouse = true;
    public NavMeshAgent agent;
    public Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        return;   
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                // Move agent
                agent.SetDestination(hit.point);
            }
            isControlledByMouse = true;
        }
    }
}
