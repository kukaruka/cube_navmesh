using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static System.Math;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    public static float t = 0.0f;
    public static bool isHitByObstacle = false;
    private Coroutine cor;
    public static bool stopCor = false;

    private Vector3 direction;
    private Vector3 desiredVelocity;
    public float tumblingDuration = 1.0f;
    private Rigidbody rb;
    private NavMeshAgent agent = null;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        tumblingDuration = 1.0f;
        agent = null;
        agent = GameObject.Find("CubeAgent").GetComponent<NavMeshAgent>();
        direction = new Vector3(0, 0, 0);
        desiredVelocity = new Vector3(0, 0, 0);
        agent.enabled = true;
    }

    void Update()
    {
        if (stopCor && cor != null)
        {
            if (cor != null)
                StopCoroutine(cor);
            stopCor = false;
            isTumbling = false;
        }

        direction = agent.destination - transform.position;
       desiredVelocity = new Vector3(agent.desiredVelocity.x, agent.desiredVelocity.y, agent.desiredVelocity.z);

        var dir  = Vector3.zero;

        if (PlayerController.isControlledByMouse && !GameOverScript.endgame)
        {
            if (Mathf.Abs(agent.remainingDistance) > 0.4)
            {
                if (Mathf.Abs(desiredVelocity.x) < Mathf.Abs(desiredVelocity.z))
                {
                    float sign = Mathf.Sign(desiredVelocity.z);
                    if (sign > 0)
                    {
                        dir = Vector3.forward; //fwd
                    }
                    else
                    {
                        dir = Vector3.back;
                    }
                }
                else
                {
                    float sign = Mathf.Sign(desiredVelocity.x);
                    if (sign > 0)
                    {
                        dir = Vector3.right;
                    }
                    else
                    {
                        dir = Vector3.left;
                    }
                }
            }
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            PlayerController.isControlledByMouse = false;
            if (!GameOverScript.endgame) {
                if (Input.GetKey(KeyCode.UpArrow))
                    dir = Vector3.forward;

                if (Input.GetKey(KeyCode.DownArrow))
                    dir = Vector3.back;

                if (Input.GetKey(KeyCode.LeftArrow))
                    dir = Vector3.left;

                if (Input.GetKey(KeyCode.RightArrow))
                    dir = Vector3.right;
            }
        }
        
        if (dir != Vector3.zero && !isTumbling && !isHitByObstacle)
        {
            if (CollisionScore.isShifting)
                tumblingDuration = 0.1f;
            else
                tumblingDuration = 0.2f;

            cor = StartCoroutine(Tumble(dir));
        }
    }

    bool isTumbling = false;
    IEnumerator Tumble(Vector3 direction)
    {
        isTumbling = true;
        var cubeSide = transform.localScale.x;

        var rotAxis = Vector3.Cross(Vector3.up, direction);
        var pivot = (transform.position + Vector3.down * transform.localScale.x/2) + direction * transform.localScale.x/2;

        var startRotation = transform.rotation;
        var endRotation = Quaternion.AngleAxis(90.0f, rotAxis) * startRotation;

        var startPosition = transform.position;
        var endPosition = transform.position + direction * transform.localScale.x;

        var rotSpeed = 90.0f / tumblingDuration;
        t = 0.0f;

        while (t < tumblingDuration)
        {
            t += Time.deltaTime;
            if (t < tumblingDuration)
            {
                rb.useGravity = false;
                transform.RotateAround(pivot, rotAxis, rotSpeed * Time.deltaTime);
                yield return null;
            }
            else
            {
                rb.useGravity = true;
                transform.rotation = endRotation;
                transform.position = endPosition;
            }
        }
        isTumbling = false;
    }
}