# cube_navmesh
 Sample Educational project within Skoltech VR & Haptics course. 

Running single executable `TheCubeGame.exe` is enough to play.

![The Cube Game](https://gitlab.com/kukaruka/cube_navmesh/-/raw/master/img/cubegame_0001.mp4)
